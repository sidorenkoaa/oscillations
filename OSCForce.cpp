#include "OSCForce.h"


//OSCForce::OSCForce(): vector(0,0,0), duration(0){}


OSCForce* OSCForce::forceWithVector(Eigen::Vector3d &vector, double duration){

    //OSCForce *force = new OSCForce;
	
    this->vector = vector;
    this->duration = duration;

	return this;
}

Eigen::Vector3d OSCForce::vectorAtTime(double time){
	
	if(time > this->duration){
		Eigen::Vector3d ourVector(0,0,0);
		return ourVector;
	}else{
		return this->vector;
	}
}
