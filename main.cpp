#include "MainModel.h"
#include <fstream>
#include <iostream>
#include <string>

int main(){

    MainModel model;

    model.beginCalculations();
	model.createGrid();
    model.calculate();

    for(int i = 0; i < 500 ;++i){
        std::string fname("out/output.csv.");
        fname += std::to_string(i);
        std::fstream file;
        file.open(fname,std::ios::app | std::ios::out);

        file << "x,y,z\n";


    }

    for(std::vector<OSCPoint*>::iterator it = model.points.begin(); it != model.points.end(); ++it){
        OSCPoint *point = *it;


        for(int i = 0; i < 500 ;++i){
            std::string fname("out/output.csv.");
            fname += std::to_string(i);
            std::fstream file;
            file.open(fname,std::ios::app | std::ios::out);

            file << point->coordinates.at(i)[0] << "," << point->coordinates.at(i)[1] << "," << point->coordinates.at(i)[2] << "\n";


        }


    }


    return 0;
}
