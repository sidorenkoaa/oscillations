#include "OSCLink.h"
#include <algorithm>

void OSCLink::createLinkFromPoint(OSCPoint *initialPoint, OSCPoint *connectedPoint, double distance)
{
	//if(!isLinkExist) return;

    OSCLink *link = new OSCLink();

    link->k = 0.1;
    link->c = 0.1;
    link->initialLenght = (initialPoint->coordinates.at(0) - connectedPoint->coordinates.at(0)).stableNorm();
	
	if(link->initialLenght > distance*1.9){
		delete link;
		return;
	}

	link->points.push_back(initialPoint);
    link->points.push_back(connectedPoint);

    initialPoint->links.push_back(link);
    connectedPoint->links.push_back(link);

} 

Eigen::Vector3d OSCLink::damperForceAtTimeStep(int step, OSCPoint &point1, OSCPoint &point2){

    Eigen::Vector3d linkVector = point2.coordinates.at(step) - point1.coordinates.at(step);

    Eigen::Vector3d projV1 = point1.velocities.at(step);
	Eigen::Vector3d projV2 = point2.velocities.at(step);
	Eigen::Vector3d damperForce(0,0,0);
	if(linkVector.squaredNorm() != 0){
		projV1 = projV1.dot(linkVector) / linkVector.squaredNorm() * linkVector;
		projV2 = projV2.dot(linkVector) / linkVector.squaredNorm() * linkVector;
		damperForce = (this->c) * (projV2 - projV1);
	}
    

	return damperForce;

}
Eigen::Vector3d OSCLink::springForceAtTimeStep(int step, OSCPoint &point1, OSCPoint &point2){

    Eigen::Vector3d linkVector = point2.coordinates.at(step) - point1.coordinates.at(step);
	double deltaLength = linkVector.norm() - this->initialLenght;
	Eigen::Vector3d springForce(0,0,0);
	if(linkVector.norm() != 0){
	    springForce = deltaLength * (this->k) * linkVector / linkVector.norm();
	}
	return springForce;

}











