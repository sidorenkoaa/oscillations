#ifndef MAINMODEL_H
#define MAINMODEL_H

#include "OSCPoint.h"
#include <vector>

class MainModel{

public:
    std::vector<OSCPoint*> points;
    double timeStep;
    int timeStepCount;

    void beginCalculations();
    void checkSurfaceCondition(Eigen::Vector3d &point);
    void createGrid();
    void calculate();

};

#endif
