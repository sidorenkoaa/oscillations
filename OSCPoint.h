#ifndef OSCPOINT_H
#define OSCPOINT_H

#include <vector>
#include "OSCLink.h"
#include "OSCForce.h"
#include <Eigen/Dense>
#include "OSCPoint.h"
class OSCLink;
class OSCPoint
{
public:
    std::vector<Eigen::Vector3d> coordinates;
    std::vector<Eigen::Vector3d> velocities;
    std::vector<OSCLink*> links;
    OSCForce *force;
    double time;
    double mass;
    bool isOutside;
};
#endif
