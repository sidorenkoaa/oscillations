#ifndef OSCLINK_H
#define OSCLINK_H

#include "OSCPoint.h"
#include <vector>
#include <Eigen/Dense>
class OSCPoint;

class OSCLink
{
public:
    std::vector<OSCPoint*> points;
    double k;
    double c;
    double initialLenght;
    bool shouldBeDrawn;

    static void createLinkFromPoint(OSCPoint *initialPoint, OSCPoint *connectedPoint, double distance);
    Eigen::Vector3d damperForceAtTimeStep(int step, OSCPoint &point1, OSCPoint &point2);
    Eigen::Vector3d springForceAtTimeStep(int step, OSCPoint &point1, OSCPoint &point2);
};
#endif
