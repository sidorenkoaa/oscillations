#include "MainModel.h"
#include "OSCForce.h"
#include "OSCLink.h"
#include "OSCPoint.h"
//#include <algorithm>

class MainModel;

void MainModel::beginCalculations(){

    this->timeStep = 1.0;
    this->timeStepCount = 500;


}

void MainModel::createGrid(){

    int xCount = 10;
    int yCount = 10;
    int zCount = 10;


    double distance = 10.0;

    //main obejcts are finalPointArray and points

    std::vector<OSCPoint*> finalPointsArray;
    std::vector< std::vector< std::vector <OSCPoint*> > > points;                                  //!!

	points.resize(xCount);
    for(int x = 0; x < xCount; x++){
		points.at(x).resize(yCount);
        for(int y = 0; y < yCount; y++){
			// no resize becouse push_back
            for(int z = 0; z < zCount; z++){
                OSCPoint *point = new OSCPoint();
				OSCForce *force = new OSCForce();
				point->force = force;
                //Задаем начальные условия

                Eigen::Vector3d tempCoord(x*distance, y*distance, z*distance);
                point->coordinates.push_back(tempCoord);

                Eigen::Vector3d tempVelo(0.0, 0.0, 0.0);
                point->velocities.push_back(tempVelo);

                point->mass = 1;

				//На силы тоже н.у.

				point->force->duration = 0;
				point->force->vector << 0,0,0;

                // И время
                point->time = 0;



                if(x == 0 || x == xCount-1 || y == 0 || y == yCount-1 || z == 0 || z == zCount-1){
                    point->isOutside = true;
                }

				

				points.at(x).at(y).push_back(point);  //!!
				finalPointsArray.push_back(point);


            }
        }
    }



    for(int x = 0; x < xCount; x++){
        for(int y = 0; y < yCount; y++){
            for(int z = 0; z < zCount; z++){

                //OSCPoint point = points.at(x+y+z);

                if(x > 0 && x < xCount){
                    OSCLink link;
                    link.createLinkFromPoint( points.at(x).at(y).at(z), points.at(x-1).at(y).at(z), distance );

                    if(x == xCount-1 || y == yCount-1 || z == zCount-1){
                        link.shouldBeDrawn = true;
                    }

                    if(y-1 >= 0){
						link.createLinkFromPoint( points.at(x).at(y).at(z),points.at(x-1).at(y-1).at(z), distance );
                    }

                    if(y+1 <yCount ){
						link.createLinkFromPoint( points.at(x).at(y).at(z),points.at(x-1).at(y+1).at(z), distance );
                    }

                }


                if(y > 0 && y < yCount){
                    OSCLink link;
					link.createLinkFromPoint( points.at(x).at(y).at(z), points.at(x).at(y-1).at(z), distance );

                    if(x == xCount-1 || y == yCount-1 || z == zCount-1){
                        link.shouldBeDrawn = true;
                    }

                    if(z-1 >= 0){
						link.createLinkFromPoint( points.at(x).at(y).at(z),points.at(x).at(y-1).at(z-1), distance );
                    }

                    if(z+1 < zCount ){
						link.createLinkFromPoint( points.at(x).at(y).at(z),points.at(x).at(y-1).at(z+1), distance );
                    }

                }


                if(z > 0 && z < zCount){

                    OSCLink link;
					link.createLinkFromPoint( points.at(x).at(y).at(z), points.at(x).at(y).at(z-1), distance );

                    if(x == xCount-1 || y == yCount-1 || z == zCount-1){
                        link.shouldBeDrawn = true;
                    }

                    if(x-1 >= 0){
						link.createLinkFromPoint( points.at(x).at(y).at(z),points.at(x-1).at(y).at(z-1), distance );
                    }

                    if(x+1 < xCount ){
						link.createLinkFromPoint( points.at(x).at(y).at(z),points.at(x+1).at(y).at(z-1), distance );
                    }

                }



            }
        }
    }
    Eigen::Vector3d vec3(0.0, -20.0 ,0.0);
    OSCForce *force = new OSCForce();
    force = force->forceWithVector(vec3, (this->timeStep)*5.0);

    points.at(5).at(9).at(5)->force = force;// need to change this when changing grid size

    this->points = finalPointsArray;

}


void MainModel::checkSurfaceCondition(Eigen::Vector3d &point){

        //[1] is y coordinate

    if(point[1] < 0){
        point[1] = 0;
    }

}


void MainModel::calculate(){

    const double g = 0.0001;

    for(int i = 1; i <= this->timeStepCount; i++){

        double currentTime = (i-1)*(this->timeStep);

		for(std::vector<OSCPoint*>::iterator it = this->points.begin(); it != this->points.end() ; ++it)
		{
			OSCPoint *point = *it;
            // Фиксируем момент времени
            point->time = currentTime;
            // Посчитаем сумму сил, действовавших на точку в момент времени i-1:

            Eigen::Vector3d forces (0.0, 0.0, 0.0);

            //for (int k = 0; k < point->links.size(); k++)
			for(std::vector<OSCLink*>::iterator itl = point->links.begin(); itl != point->links.end() ; ++itl){
                OSCLink *link = *itl;
				OSCPoint *connectedPoint = link->points.front() != point ? link->points.front() : link->points.back();

                // Сила пружины:
                forces += link->springForceAtTimeStep(i-1,*point,*connectedPoint);

                // Сила демпфера:
                forces += link->damperForceAtTimeStep(i-1,*point,*connectedPoint);
            }

            // Добавим силу, если она была:
            forces += point->force->vectorAtTime(currentTime);
            Eigen::Vector3d mg (0,-point->mass * g ,0);
            forces +=mg;
            // Теперь посчитаем изменение скорости:

            Eigen::Vector3d deltaVelocity = forces * (this->timeStep) / point->mass;
            Eigen::Vector3d previousVelocity = point->velocities.at(i-1);
            Eigen::Vector3d newVelocity = previousVelocity + deltaVelocity;
            point->velocities.push_back(newVelocity);

            // И изменение координат

            Eigen::Vector3d deltaCoordinates = newVelocity * (this->timeStep);
            Eigen::Vector3d newCoordinates = point->coordinates.at(i-1) + deltaCoordinates;
            this->checkSurfaceCondition(newCoordinates);
            point->coordinates.push_back(newCoordinates);

        }

    }

}













