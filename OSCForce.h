#ifndef OSCFORCE_H
#define OSCFORCE_H

#include <Eigen/Dense>

class OSCForce
{
public:
	Eigen::Vector3d vector;
	double duration;

    //OSCForce();
	OSCForce* forceWithVector(Eigen::Vector3d &vector, double duration);
	Eigen::Vector3d vectorAtTime(double time);
};
#endif
